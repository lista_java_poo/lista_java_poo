package Exercicio_06;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Agenda agenda = new Agenda();

        // Adicionando alguns contatos à agenda
        agenda.adicionarContato(new Contato("João", "123456789"));
        agenda.adicionarContato(new Contato("Maria", "987654321"));
        agenda.adicionarContato(new Contato("José", "555666777"));

        // Buscando um contato por nome
        List<Contato> contatosEncontrados = agenda.buscarContatosPorNome("Maria");
        if (contatosEncontrados.isEmpty()) {
            System.out.println("Nenhum contato encontrado com esse nome.");
        } else {
            System.out.println("Contato(s) encontrado(s):");
            for (Contato contato : contatosEncontrados) {
                System.out.println("Nome: " + contato.getNome() + ", Telefone: " + contato.getTelefone());
            }
        }
    }
}
