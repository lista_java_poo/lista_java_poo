package Exercicio_03;

public class Main {

    public static void main(String[] args) {

        ContaBancaria conta1 = new ContaBancaria(123456, "João", 1000);
        ContaBancaria conta2 = new ContaBancaria(789012, "Maria", 500);

        conta1.depositar(200);
        conta2.sacar(100);

        System.out.println("Saldo da conta de " + conta1.getNomeTitular() + ": R$ " + conta1.getSaldo());
        System.out.println("Saldo da conta de " + conta2.getNomeTitular() + ": R$ " + conta2.getSaldo());
    }

}
