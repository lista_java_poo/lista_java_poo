package Exercicio_04;

public class Main {

    public static void main(String[] args) {
        
        Jogador jogador1 = new Jogador("Jorge", 1000, 1);
        Jogador jogador2 = new Jogador("Eduardo", 1500, 3 );

        System.out.println("Informações do jogador:");
        jogador1.exibirInformacoes();
        System.out.println();
        jogador2.exibirInformacoes();
        System.out.println("\n\n");

        jogador1.aumentarPontuacao(500);
        jogador2.aumentarPontuacao(300);

        jogador1.subirNivel();
        jogador2.subirNivel();

        System.out.println("Informações atualizadas do jogador:");
        jogador1.exibirInformacoes();
        System.out.println();
        jogador2.exibirInformacoes();
    }

}
